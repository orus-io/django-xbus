# django-xbus

[![Build Status](https://drone.xcg.io/api/badges/xcg/django-xbus/status.svg)](https://drone.xcg.io/xcg/django-xbus)

Django addon that connects to [Xbus](https://bitbucket.org/orus-io/xbus).

This addon handles both message emission and message consumption via
[Xbus-HTTP](https://bitbucket.org/orus-io/xbus-http).


## Documentation

```
$ pip install -r requirements.txt
$ pip install -r doc/requirements.txt
$ cd doc
$ make html
```

## Tests

In order to run tests you must have a python3 environment with
[python-xbus](https://pypi.org/project/python-xbus/)
`tests/xbus-test-replier` installed.

```
$ python3 -m venv ./py3-venv
$ ./py3-venv/bin/pip install python-xbus
$ ./py3-venv/bin/pip install ./tests/xbus-test-replier
```

Now export your xbus python client:

```
$ XBUS_PYTHON_CLIENT=$PWD/py3-venv/bin/xbus-python-client
```

You also need a postgres database for xbus, you can use the following
docker compose file:

```yaml
xbus_postgres:
  image: postgres:9.6-alpine
  ports:
    - 5432:5432
  environment:
    - POSTGRES_PASSWORD=xbus
    - POSTGRES_USER=xbus
    - POSTGRES_DB=xbus
```

And run `docker-compose up -d` to launch it. Then export your postgres dsn:

```
$ export XBUS_TEST_POSTGRES_DSN="dbname=xbus user=xbus password=xbus sslmode=disable"
```

Xbus binaries needs to be present in your path:

```
$ export PATH=$PATH:$PWD/xbus_bin
```

Now let `tox` run tests:

```
$ pip install tox  # assuming pip is for python2
$ tox
```
