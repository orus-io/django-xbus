#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time

import requests

from django.test import TestCase

from .fullenv import Fullenv


class TestFullenv(TestCase):
    def test_startup_shutdown(self):
        path = os.path.join(os.path.dirname(__file__), "fullenv.yml")
        fe = Fullenv(path)
        with fe, fe.up():
            time.sleep(0.5)  # let HTTP Gateway time to warm up
            res = requests.get("http://127.0.0.1:8911")
            self.assertEqual(res.status_code, 404)
