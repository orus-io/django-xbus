# -*- coding: utf-8 -*-
# Import from the Standard Library.
import json
import os
import tempfile
import time
from base64 import b64decode, b64encode

# Import from dependencies.
import requests
import yaml

from django.core.management import call_command
from django.test import TestCase, TransactionTestCase

from .fullenv import Fullenv

import mock
import requests_mock
from tests.models import (  # Consumer,
    EmitterWithExitCondition,
    EmitterWithoutMethod,
    SimpleEmitter,
    SimpleFailEmitter
)
from xbus.api import send_event
from xbus.context_manager import disconnect_xbus_send
from xbus.models import Envelope, Event

# import uuid


class TestEmitterWithoutMethod(TestCase):
    """To test emitter without method"""

    def test_without_method(self):
        with self.assertRaises(NotImplementedError):
            EmitterWithoutMethod.objects.create(name="Try")


class TestSimpleEmitter(TransactionTestCase):
    """To test simple emitter"""

    @mock.patch("requests.post")
    def test_simple_emission(self, requests_post_mock):
        """Try a simple emission. The Xbus request part is mocked.
        """

        requests_post_mock.return_value = mock.Mock(
            status_code=requests.codes.ok, text="Test reply"
        )

        count = Envelope.objects.count()
        emitter = SimpleEmitter.objects.create(name="Try")

        call_command("xbus_queue")

        # Ensure a POST has been attempted with expected parameters.
        requests_post_mock.assert_called_once_with(
            u"http://127.0.0.1:8911/django-emitter/output",
            json=mock.ANY,
            headers={
                "Content-Type": "application/json",
                "Xbus-API-Key": u"django-api-key",
            },
        )

        # Check the envelope data that has been sent.
        # Should contain 1 event with 1 item.
        # 1: kwargs of "call_args".
        data = requests_post_mock.call_args[1]["json"]
        data_events = data["envelope"]["events"]
        self.assertEqual(len(data_events), 1)
        data_items = data_events[0]["items"]
        self.assertEqual(len(data_items), 1)
        self.assertEqual(json.loads(b64decode(data_items[0]))["name"], "Try")

        envelope = Envelope.objects.last()
        refresh_count = Envelope.objects.count()

        self.assertEqual(emitter.name, "Try")
        self.assertEqual(envelope.state, "done")
        self.assertEqual(refresh_count, count + 1)

    def test_simple_emission_with_fullenv(self):
        SimpleEmitter.objects.create(name="Try")

        path = os.path.join(
            os.path.dirname(__file__), "fullenv_test_emitter.yml"
        )
        fe = Fullenv(path)
        with fe, fe.up():
            time.sleep(0.5)  # let HTTP Gateway time to warm up
            call_command("xbus_queue")
            time.sleep(0.5)  # let HTTP Gateway time to send to consumer
            res = fe.xbusctl("ps", "export")

        res = "".join(res[1:])
        res = yaml.load(res)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]["status"], "DONE")

    def test_simple_emission_fail_with_fullenv(self):
        SimpleFailEmitter.objects.create(name="Try")

        path = os.path.join(
            os.path.dirname(__file__), "fullenv_test_emitter.yml"
        )
        fe = Fullenv(path)
        with fe, fe.up():
            time.sleep(0.5)  # let HTTP Gateway time to warm up
            call_command("xbus_queue")
            time.sleep(0.5)  # let HTTP Gateway time to send to xbus
            res = fe.xbusctl("ps", "export")

        res = "".join(res[1:])
        res = yaml.load(res)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]["status"], "ERROR")

    def test_failed_emission(self):
        """Try a simple emission. Mock a false url here so it should fail.
        """

        count = Envelope.objects.count()
        emitter = SimpleEmitter.objects.create(name="Try")

        with requests_mock.Mocker() as m:
            m.get("http://127.0.0.1")
            call_command("xbus_queue")

        envelope = Envelope.objects.last()
        refresh_count = Envelope.objects.count()

        self.assertEqual(emitter.name, "Try")
        self.assertEqual(envelope.state, "error")
        self.assertEqual(refresh_count, count + 1)

    def test_disconnect_xbus_send(self):

        count = Envelope.objects.count()

        with disconnect_xbus_send():
            emitter = SimpleEmitter.objects.create(name="Try")

        with requests_mock.Mocker() as m:
            m.get("http://127.0.0.1")
            call_command("xbus_queue")

        refresh_count = Envelope.objects.count()

        self.assertEqual(emitter.name, "Try")
        self.assertEqual(refresh_count, count)

    @disconnect_xbus_send()
    def test_disconnect_xbus_send_with_decorator(self):

        count = Envelope.objects.count()
        emitter = SimpleEmitter.objects.create(name="Try")

        with requests_mock.Mocker() as m:
            m.get("http://127.0.0.1")
            call_command("xbus_queue")

        refresh_count = Envelope.objects.count()

        self.assertEqual(emitter.name, "Try")
        self.assertEqual(refresh_count, count)


class TestEmitterWithExitCondition(TestCase):
    """To test emitter with exit condition"""

    def test_with_exit_condition(self):

        count = Envelope.objects.count()
        emitter = EmitterWithExitCondition.objects.create(name="Try")

        with requests_mock.Mocker() as m:
            m.post("http://127.0.0.1:8911/django-emitter/output", text="OK")
            call_command("xbus_queue")

        refresh_count = Envelope.objects.count()

        self.assertEqual(emitter.name, "Try")
        self.assertEqual(refresh_count, count)


class TestConsumer(TransactionTestCase):
    """Message consumption tests."""

    def test_consumer_with_fullenv(self):
        req_data = {
            "events": [
                {
                    "ItemCount": 1,
                    "encoding": "binary",
                    "items": [
                        b64encode(
                            json.dumps(
                                {
                                    "xref": "fake",
                                    "xbus_message_correlation_id": "fake",
                                }
                            )
                        )
                    ],
                    "type": "test.to.django",
                }
            ]
        }

        req_data = json.dumps(req_data)
        emitter = "client-test-emitter"
        count = Event.objects.count()

        path = os.path.join(
            os.path.dirname(__file__), "fullenv_test_emitter.yml"
        )
        fe = Fullenv(path)
        with fe, fe.up(), tempfile.NamedTemporaryFile() as temp:
            temp.write(req_data)
            temp.seek(0)
            time.sleep(0.5)  # let HTTP Gateway time to warm up
            res = fe.xbus_client(
                emitter, "emit", "test-emitter", stdin=temp.name
            )
            time.sleep(0.5)  # let HTTP Gateway time to send to xbus
            res = fe.xbusctl("ps", "export")

            res = "".join(res[1:])
            res = yaml.load(res)
            self.assertEqual(len(res), 1)
            self.assertEqual(res[0]["status"], "RUNNING")

            call_command("xbus_consumer", max_runs=1)
            self.assertEqual(Event.objects.count(), count + 1)

            res = fe.xbusctl("ps", "export")
            res = "".join(res[1:])
            res = yaml.load(res)
            self.assertEqual(len(res), 1)
            self.assertEqual(res[0]["status"], "DONE")

    def test_consumer_fail_with_fullenv(self):
        """requested data doesn't contain xref so it should fail"""
        req_data = {
            "events": [
                {
                    "ItemCount": 1,
                    "encoding": "binary",
                    "items": [
                        b64encode(
                            json.dumps({"xbus_message_correlation_id": "fake"})
                        )
                    ],
                    "type": "test.to.django",
                }
            ]
        }

        req_data = json.dumps(req_data)
        emitter = "client-test-emitter"
        count = Event.objects.count()

        path = os.path.join(
            os.path.dirname(__file__), "fullenv_test_emitter.yml"
        )
        fe = Fullenv(path)
        with fe, fe.up(), tempfile.NamedTemporaryFile() as temp:
            temp.write(req_data)
            temp.seek(0)
            time.sleep(0.5)  # let HTTP Gateway time to warm up
            res = fe.xbus_client(
                emitter, "emit", "test-emitter", stdin=temp.name
            )
            time.sleep(0.5)  # let HTTP Gateway time to send to xbus
            res = fe.xbusctl("ps", "export")

            res = "".join(res[1:])
            res = yaml.load(res)
            self.assertEqual(len(res), 1)
            self.assertEqual(res[0]["status"], "RUNNING")

            call_command("xbus_consumer", max_runs=1)
            self.assertEqual(Event.objects.count(), count)

            res = fe.xbusctl("ps", "export")
            res = "".join(res[1:])
            res = yaml.load(res)
            self.assertEqual(len(res), 1)
            self.assertEqual(res[0]["status"], "ERROR")


class InstanceMock(object):
    def __init__(self):
        self.xref = "fake"
        self.pk = None


class TestImmediateReply(TransactionTestCase):
    """Message consumption tests."""

    def test_immediate_reply_with_fullenv(self):

        path = os.path.join(
            os.path.dirname(__file__), "fullenv_test_emitter.yml"
        )
        fe = Fullenv(path)

        with fe, fe.up():
            time.sleep(0.5)  # let HTTP Gateway time to warm up

            instance = InstanceMock()
            event, success, reply = send_event(
                instance,
                "request.ping",
                {"foo": "bar"},
                immediate=True,
                run_command=True,
            )

            xbus_ps = fe.xbusctl("ps", "export")

        self.assertEqual(success, True)
        self.assertEqual(event.direction, "immediate-out")
        self.assertEqual(
            event.xbus_message_correlation_id,
            reply.get("xbus_message_correlation_id"),
        )
        self.assertEqual(event.xref, reply.get("xref"))
        self.assertEqual(reply["some"], "data")

        xbus_ps = "".join(xbus_ps[1:])
        xbus_ps = yaml.load(xbus_ps)
        self.assertEqual(len(xbus_ps), 1)
        self.assertEqual(xbus_ps[0]["status"], "DONE")
