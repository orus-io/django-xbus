#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

from django.test import TransactionTestCase

import requests_mock
from xbus.api import _xbus_send_event
from xbus.models import Envelope, Event

requests_mock.mock.case_sensitive = True


class ModelMock(object):
    def __init__(self, *args, **kwargs):
        self.xref = kwargs.get("xref", "")
        self.pk = kwargs.get("pk", "")
        for k, v in kwargs:
            setattr(self, k, v)

    def save(self, *args, **kwargs):
        pass


class TestXbusAPI(TransactionTestCase):

    item = {"this": "is a simple django test"}

    def get_xbus_http_matcher(self):
        def xbus_http_matcher(request):
            # Check headers
            self.assertIn("Content-Type", request.headers)
            self.assertEqual(
                request.headers["Content-Type"], "application/json"
            )
            self.assertIn("Xbus-API-Key", request.headers)
            self.assertEqual(request.headers["Xbus-API-Key"], "django-api-key")

            # Check payload
            payload = request.json()
            self.assertIn("envelope", payload)
            self.assertIn("output", payload)
            self.assertEqual(payload["output"], "default")

            # Check payload envelope
            envelope = payload["envelope"]
            self.assertIn("events", envelope)
            self.assertEqual(len(envelope["events"]), 1)

            # Check payload envelope event
            event = envelope["events"][0]
            self.assertIn("ItemCount", event)
            self.assertEqual(event["ItemCount"], 1)

            self.assertIn("encoding", event)
            self.assertEqual(event["encoding"], "binary")

            self.assertIn("items", event)
            self.assertEqual(len(event["items"]), 1)

            self.assertEqual(
                event["items"][0],
                # base64 encoded version of self.item
                u"eyJ0aGlzIjogImlzIGEgc2ltcGxlIGRqYW5nbyB0ZXN0In0=",
            )

            self.assertIn("type", event)
            self.assertEqual(event["type"], "from-django.msg")

            return True

        return xbus_http_matcher

    def test_xbus_send_event_success(self):
        envelope = Envelope.objects.create(direction="out", state="draft")
        Event.objects.create(
            event_type="from-django.msg",
            envelope=envelope,
            item=json.dumps(self.item),
        )

        with requests_mock.Mocker() as m:
            m.post(
                "http://127.0.0.1:8911/django-emitter/output",
                text="OK",
                additional_matcher=self.get_xbus_http_matcher(),
            )
            success, reply = _xbus_send_event(envelope)

        self.assertEqual("OK", reply)
        self.assertEqual(True, success)
