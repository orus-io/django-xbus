# utilities to setup a full testing environment.
import os
import subprocess
import sys


class Error(RuntimeError):
    pass


class FullenvRunContext:
    def __init__(self, env):
        self.env = env

    def __enter__(self):
        self.env.startup()

    def __exit__(self, exc_type, exc, tb):
        self.env.shutdown()


class Fullenv:
    def __init__(self, config):
        self.config = config
        self.process = None

    def _read_stderr(self):
        while True:
            line = self.process.stderr.readline()
            if not line:
                return
            sys.stderr.buffer.write(line)

    def _next_output(self):
        while True:
            line = self.process.stdout.readline()
            if not line:
                raise Error("Unexpected EOF")
            line = line.decode("utf-8").strip("\n")
            if not line.startswith("< "):
                continue
            if line.startswith("< OK: "):
                return line[6:]
            if line == "< OK":
                return None
            if line.startswith("< ERR: "):
                raise Error(line[7:])
            raise Error("Unexpected reply: %s" % line)

    def _run_command(self, *args):
        cmd = " ".join(args) + "\n"
        self.process.stdin.write(cmd.encode("utf-8"))
        self.process.stdin.flush()
        return self._next_output()

    def init(self):
        self.process = subprocess.Popen(
            [
                "xbus-fullenv",
                "run",
                "--no-prompt",
                "--var",
                "XBUS_PYTHON_CLIENT=" + os.getenv("XBUS_PYTHON_CLIENT"),
                self.config,
            ],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
        self.workdir = self._next_output()

    def close(self):
        try:
            self._run_command("quit")
        except Exception:
            pass
        self.process.wait()
        self.process = None

    def __enter__(self):
        self.init()

    def __exit__(self, exc_type, exc, tb):
        self.close()

    def startup(self):
        self._run_command("startup")

    def shutdown(self):
        self._run_command("shutdown")

    def up(self):
        return FullenvRunContext(self)

    def client_config(self, clientname):
        return self._run_command("client-config", clientname)

    def xbusctl(self, *args):
        process = subprocess.Popen(
            ["xbusctl", "--config", self.workdir + "/xbusctl/xbusctl.yaml"]
            + list(args),
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
        process.wait()
        return process.stdout.readlines()

    def xbus_client(self, client_name, *args, **kwargs):
        stdin = kwargs.get("stdin")
        if stdin is not None:
            with open(stdin, "rb") as _stdin:
                process = subprocess.Popen(
                    [
                        "xbus-client",
                        "--config",
                        self.workdir + "/" + client_name + "/xbus-client.yaml",
                    ]
                    + list(args),
                    stdin=_stdin,
                    stdout=subprocess.PIPE,
                )
        else:
            process = subprocess.Popen(
                [
                    "xbus-client",
                    "--config",
                    self.workdir + "/" + client_name + "/xbus-client.yaml",
                ]
                + list(args),
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
            )
        process.wait()
        return process.stdout.readlines()
