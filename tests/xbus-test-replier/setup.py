#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup

setup(name="xbus-test-replier", py_modules=["xbus_test_replier"])
