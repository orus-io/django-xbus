#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import sys


class Replier:
    def __init__(self, actor):
        self.actor = actor

    async def process(self, apc):
        """apc: Actor Process Context"""
        envelope = await apc.read_envelope_complete("request", 1)
        data = json.loads(envelope.events[0].items[0])
        event_type = envelope.events[0].type + ".reply"
        # hint: response is in enveloppe
        output = apc.open_output("reply", [event_type])
        item = json.dumps(
            {
                "some": "data",
                "xref": data["xref"],
                "xbus_message_correlation_id": data[
                    "xbus_message_correlation_id"
                ],
            }
        ).encode()
        await output.add_items(event_type, item)
        await output.close()
