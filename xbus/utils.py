"""
Defined some utility functions to work with this module.
"""
# Imports from django
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist

# Import from xbus
from xbus import api


def update_object(instance, **kwargs):
    """
    Helper function to update an object with the given new values.

    Instance is saved only if at least one field is updated.

    Args:
        instance (xbus.models.XbusAwareMixin): instance to update.
        kwargs: fields name and value as arguments.

    Returns:
        bool: True if updated else False
    """
    updated = False
    for key, value in kwargs.items():
        old_value = getattr(instance, key)
        if value != old_value:
            setattr(instance, key, value)
            updated = True

    if updated:
        instance.save()

    return updated


def kwargs_from_dict(item, keys, map_fields={}):
    """
    Helper function that map xbus field names to other ones.

    Usefull if xbus recieved fields' names are different than the one use for
    django model.

    Args:
        item (dict): xbus recieved item.
        keys (list of str): list of key you want to preserve, other ones are
            ignored.
        map_fields (dict str->str): map xbus field name to django field name.

    Returns:
        dict: django field names to xbus values.
    """
    kw = {}
    for key in keys:
        if key in item:
            kw[map_fields.get(key, key)] = item[key]

    return kw


def kwargs_from_instance(instance, fields, map_fields):
    """
    Helper function that map django instance fields' names to other ones.

    Usefull if django fields' names are different than the one use for xbus.

    Args:
        instance (xbus.models.XbusAwareMixin): current instance.
        fields (list of str): list of fields you want to send, other ones are
            ignored.
        map_fields (dict str->str): map django field name to xbus field name.

    Returns:
        dict: django field names to xbus values.
    """
    kw = {}
    for field in fields:
        key = map_fields.get(field, field)
        kw[field] = getattr(instance, key)

    return kw


def has_updated_fields(updated_fields, fields, map_fields={}):
    """
    Check if some the fields we track the info are in the list of updated
    fields

    Args:
        updated_fields (list of str): list of updated fields.
        fields (list of str): list of tracked fields.
        map_fields (dict str->str): map django field name to xbus field name.
    Returns:
        bool: True if at least one field is updated
    """

    # If the list of update fields are empty we can't known the exact update
    # fields, then we consider that the fields we track has been updated
    if not updated_fields:
        return True

    for field in fields:
        key = map_fields.get(field, field)
        if key in updated_fields:
            return True

    return False


def get_handler(event_type):
    """Retrieve handler for given event type.

    Args:
        event_type (str): Xbus Event Type.

    Returns:
        func: callback function
    """

    return api.registry.get(event_type)


def process_incoming_data(event_type, xbus_dict):
    """Apply callback function for a given event type.

    Args:
        event_type (str): Xbus event type.
        xbus_dict (dict): xbus items recieved from xbus.

    Returns:
        callback function returned value.
    """
    xref = xbus_dict["xref"]
    handler = get_handler(event_type)
    if handler is None:
        raise Exception("unknown event_type: %s" % event_type)

    return handler(xref, xbus_dict)


def get_object(model, **kw):
    """
    Helper function that returns the object asked for, or None if it is not
    found.

    In case of failure error message is going to be formated to provide extra
    informations.

    Args:
        model (xbus.models.XbusAwareMixin): Django model.
        kw: keyword arguments pass to `model.objects.get` method.

    Returns:
        xbus.models.XbusAwareMixin: retrieved instance.
    """
    try:
        return model.objects.get(**kw)
    except ObjectDoesNotExist:
        return None
    except MultipleObjectsReturned:
        absolute_module_path = ".".join((model.__module__, model.__name__))
        kw_list = ", ".join(["%s=%s" % (key, val) for key, val in kw.items()])
        message = (
            "The following query : %s.get(%s), should have returned only "
            "one result, but instead returned multiple objects:\n"
        ) % (absolute_module_path, kw_list)

        total = 0
        for obj in model.objects.filter(**kw):
            if total > 8:
                message += "- ..."
                break

            if hasattr(obj, "get_admin_url"):
                url = obj.get_admin_url()
            message += "- %s\n" % url
            total += 1

        raise MultipleObjectsReturned(message)
