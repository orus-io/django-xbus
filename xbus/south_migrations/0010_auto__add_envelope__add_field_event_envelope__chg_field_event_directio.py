# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Envelope'
        db.create_table(u'xbus_envelope', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True)),
            ('envelope_id', self.gf('django.db.models.fields.CharField')(max_length=80, null=True, blank=True)),
            ('direction', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'xbus', ['Envelope'])

        # Adding field 'Event.envelope'
        db.add_column(u'xbus_event', 'envelope',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['xbus.Envelope'], null=True),
                      keep_default=False)


        # Changing field 'Event.direction'
        db.alter_column(u'xbus_event', 'direction', self.gf('django.db.models.fields.CharField')(max_length=25, null=True))

        # Changing field 'Event.item'
        db.alter_column(u'xbus_event', 'item', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Event.state'
        db.alter_column(u'xbus_event', 'state', self.gf('django.db.models.fields.CharField')(max_length=20, null=True))

    def backwards(self, orm):
        # Deleting model 'Envelope'
        db.delete_table(u'xbus_envelope')

        # Deleting field 'Event.envelope'
        db.delete_column(u'xbus_event', 'envelope_id')


        # Changing field 'Event.direction'
        db.alter_column(u'xbus_event', 'direction', self.gf('django.db.models.fields.CharField')(default='out', max_length=25))

        # Changing field 'Event.item'
        db.alter_column(u'xbus_event', 'item', self.gf('django.db.models.fields.BinaryField')())

        # Changing field 'Event.state'
        db.alter_column(u'xbus_event', 'state', self.gf('django.db.models.fields.CharField')(default='draft', max_length=20))

    models = {
        u'xbus.envelope': {
            'Meta': {'object_name': 'Envelope'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'envelope_id': ('django.db.models.fields.CharField', [], {'max_length': '80', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'xbus.event': {
            'Meta': {'object_name': 'Event'},
            'admin_url': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250', 'null': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'ctime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True'}),
            'envelope': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['xbus.Envelope']", 'null': 'True'}),
            'event_id': ('django.db.models.fields.CharField', [], {'max_length': '80', 'null': 'True', 'blank': 'True'}),
            'event_type': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'xbus_message_correlation_id': ('django.db.models.fields.CharField', [], {'max_length': '36'}),
            'xref': ('django.db.models.fields.CharField', [], {'max_length': '80'})
        }
    }

    complete_apps = ['xbus']