# -*- coding: utf-8 -*-
import os

from django.conf import settings
from django.utils.translation import ugettext as _

XREF_LENGTH = 80
DIRECTION_CHOICES = (
    ("in", _(u"Incoming")),
    ("out", _(u"Outgoing")),
    ("immediate-out", _(u"Immediate-Out")),
)
STATE_CHOICES = (
    ("draft", _(u"Draft")),
    ("pending", _(u"Pending")),
    ("done", u"Done"),
    ("error", _(u"Error")),
)

# A token used to log into Xbus via Xbus-HTTP.
XBUS_API_KEY = getattr(settings, "XBUS_API_KEY", os.getenv("XBUS_API_KEY"))

# The URL used to reach Xbus-HTTP. Should have no trailing slash. Should be of
# the form: <scheme>://<host>:<port>
XBUS_HTTP_URL = getattr(settings, "XBUS_HTTP_URL", os.getenv("XBUS_HTTP_URL"))

# Name of actors used to emit data to / consume data from Xbus-HTTP.
XBUS_EMITTER_NAME = getattr(
    settings, "XBUS_EMITTER_NAME", os.getenv("XBUS_EMITTER_NAME")
)
XBUS_CONSUMER_NAME = getattr(
    settings, "XBUS_CONSUMER_NAME", os.getenv("XBUS_CONSUMER_NAME")
)

# Name of the output channel used in emissions (usually "default").
XBUS_EMITTER_OUTPUT = getattr(
    settings, "XBUS_EMITTER_OUTPUT", os.getenv("XBUS_EMITTER_OUTPUT")
)

# The URL used to reach Xbus-HTTP to emit messages. Of the form:
# <scheme>://<host>:<port>/<actor-name>/output/<output-name>
XBUS_EMITTER_URL = "%s/%s/output" % (XBUS_HTTP_URL, XBUS_EMITTER_NAME)

# The URL used to reach Xbus-HTTP to receive messages. Of the form:
# <scheme>://<host>:<port>/<actor-name>/input
XBUS_CONSUMER_URL = "%s/%s/processrequest" % (
    XBUS_HTTP_URL,
    XBUS_CONSUMER_NAME,
)

# The URL used to reach Xbus-HTTP to signal processing success / failure after
# messages have been received and processed. Of the form:
# <scheme>://<host>:<port>/<actor-name>/processingend
XBUS_CONSUMER_PROCEND_URL = "%s/%s/processingend" % (
    XBUS_HTTP_URL,
    XBUS_CONSUMER_NAME,
)
