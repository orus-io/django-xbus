"""
Django models used to store Xbus data.

Also provide a mixin to simplify xbus django models synchronization on save and
update.
"""
import logging

# Other
from django_extensions.db.fields import UUIDField

# Import from Django
from django.db import models
from django.db.models import (
    CharField,
    DateTimeField,
    Manager,
    Model,
    TextField
)
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext as _

from xbus.api import send_event
from xbus.constants import DIRECTION_CHOICES, STATE_CHOICES, XREF_LENGTH

logger = logging.getLogger(__name__)


class XbusManager(Manager):
    """Django model manager subclass to retrieve models by `xref`"""

    def get_by_natural_key(self, xref, odoo_created):
        """Overload manager to retrieve by `xref` instead of `id`

        Args:
            xref (str): instance xref.
            odoo_created (bool): **DEPRECATED**.

        Returns:
            xbus.models.XbusAwareMixin: instance
        """
        # TODO: remove `odoo_created`
        return self.get(xref=xref)


class XbusAwareMixin(Model):
    """
    Base class for models to be synchronised through Xbus.
    """

    # TODO
    # 1. Add data migrations to set xref for existing objects
    # 2. Then set unique=True
    xref = UUIDField(
        _(u"External Ref"), null=True, blank=True, max_length=XREF_LENGTH
    )
    objects = XbusManager()

    class Meta:
        abstract = True

    def natural_key(self):
        """Retrieve natural keys for xbus.models.XbusManager"""
        return self.xref

    def get_xbus_fields(self):
        """Should be implemeted by child

        Returns:
            dict: default values to send to xbus
        """
        raise NotImplementedError()

    def get_xbus_event_type(self, event_type=""):
        """Returns the event_type which will be used to contact xbus

        Args:
            event_type (str): The type "created" or "updated"

        Return:
            str: Xbus pipeline event type
        """
        raise NotImplementedError()

    def get_admin_url(self):
        """
        Returns:
            str: Instance admin url
        """
        raise NotImplementedError()

    def condition_to_exit(self, created, update_fields=None):
        """Add a specific condition to exit send process, if True then message
        will not be send.

        Return:
            bool: The result of condition
        """
        return False


class Envelope(models.Model):
    """Store
    `Xbus Envelope <https://docs.xbus.io/latest/api/xbus.html#protobuf.Envelope>`_
    """  # noqa

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    envelope_id = models.CharField(max_length=80, null=True, blank=True)
    direction = models.CharField(max_length=25, choices=DIRECTION_CHOICES)
    state = models.CharField(max_length=20, choices=STATE_CHOICES)

    def __unicode__(self):
        return str(self.pk)


class Event(Model):
    """Store
    `Xbus Event <https://docs.xbus.io/latest/api/xbus.html#protobuf.Event>`_
    """

    ctime = DateTimeField(auto_now_add=True, null=True)

    # Identify the object in the database and its version
    xref = CharField(_(u"External Ref"), max_length=XREF_LENGTH)
    xbus_message_correlation_id = CharField(
        _(u"Message correlation id"), max_length=36
    )

    # Event type
    event_type = CharField(_(u"Event type"), max_length=80)
    event_id = CharField(_(u"Event id"), max_length=80, null=True, blank=True)
    direction = models.CharField(
        max_length=25, choices=DIRECTION_CHOICES, null=True
    )
    state = models.CharField(max_length=20, choices=STATE_CHOICES, null=True)
    comment = TextField(_(u"Comment"), blank=True)

    item = TextField(_(u"Event item"), blank=True)
    admin_url = CharField(
        max_length=250, default="", editable=False, null=True
    )
    envelope = models.ForeignKey(Envelope, null=True)

    def __unicode__(self):
        return str(self.pk)


@receiver(post_save, dispatch_uid="send_to_xbus")
def send_to_xbus(sender, instance, created, update_fields=None, **kwargs):
    """Send the event to xbus :class:`xbus.models.XbusAwareMixin` on post save.
    """
    if issubclass(sender, XbusAwareMixin):
        if instance.condition_to_exit(created, update_fields=update_fields):
            return

        xbus_fields = instance.get_xbus_fields()
        admin_url = instance.get_admin_url()

        if created:
            event_type = instance.get_xbus_event_type("created")
            logger.info(
                u"A new item is created and synchronize {xref}".format(
                    xref=instance.xref
                )
            )
        else:
            event_type = instance.get_xbus_event_type("updated")
            logger.info(
                u"A new item is updated and synchronize {xref}".format(
                    xref=instance.xref
                )
            )
        send_event(instance, event_type, xbus_fields, admin_url=admin_url)
