"""
Context manager utilities for Xbus.
"""
from contextlib2 import contextmanager

from django.db import transaction
from django.db.models import signals

from xbus.models import send_to_xbus


@contextmanager
def disconnect_xbus_send():
    """Use it to deactivate the signal that send xbus event on save"""
    with transaction.atomic():
        signals.post_save.disconnect(send_to_xbus, dispatch_uid="send_to_xbus")
        yield
        signals.post_save.connect(send_to_xbus, dispatch_uid="send_to_xbus")
