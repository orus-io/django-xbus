"""
Management command to recieve xbus messages from xbus-http.
"""
# Import from the Standard Library
import json
import logging
from base64 import b64decode
from optparse import make_option
from time import sleep

# Import from dependencies.
import requests

# Import from Django
from django.core.management.base import NoArgsCommand
from django.utils import timezone

# Import from this addon.
from xbus.constants import (
    XBUS_API_KEY,
    XBUS_CONSUMER_PROCEND_URL,
    XBUS_CONSUMER_URL
)
from xbus.models import Event

log = logging.getLogger("xbus.commands")


class Command(NoArgsCommand):
    """Send Xbus-HTTP GET requests in a loop and store them in database.

    help::

        Usage: ./manage.py xbus_consumer [options]

        Options:
          --max_runs            defined how much envelope should be
                                retrived before returning
          --version             show program's version number and exit
          -h, --help            show this help message and exit

    Even with `max_run` calls will block until data is available.
    """

    option_list = NoArgsCommand.option_list + (
        make_option(
            "--max_runs",
            type="int",
            default=0,
            help="defined how much envelope should be retrived before "
            "returning",
        ),
    )

    def handle_noargs(self, **kw):
        """
        Args:
            max_runs (int): The max amount of GET requests to receive data.
                Infinite by default, but may be restrained (useful during
                tests).
        """
        try:
            self.start(**kw)
        except KeyboardInterrupt:
            log.info("Stopped xbus consumer")

    def start(self, **kw):
        log.info("Starting xbus consumer")

        max_runs = kw["max_runs"] or None

        # Prepare headers for GET / POST requests towards Xbus-HTTP.
        headers = {"Xbus-API-Key": XBUS_API_KEY}

        run_count = 0

        while (not max_runs and True) or (max_runs and run_count < max_runs):
            log.info("Calling Xbus to receive data...")

            try:
                req = requests.get(XBUS_CONSUMER_URL, headers=headers)
                req.raise_for_status()
                if req.status_code == requests.codes.no_content:
                    # timeout
                    continue
                data = req.json()

            except requests.exceptions.RequestException as e:
                log.error("Could not receive data from Xbus: %s", e.message)
                sleep(1)  # Wait 1 second after an error to avoid flooding...
                continue

            log.info("Got data!")
            log.debug("Received: %s", data)

            try:
                # Save the data in the DB; a separate runner will process it.
                for _input in data.get("inputs", []):
                    for event in _input.get("envelope", {}).get("events"):
                        self.handle_data(event)
            except Exception as e:
                log.error("Could not save received data: %s", e.message)
                self.send_confirmation(data, "error")
                continue
            else:
                log.info("Saved the data into the DB.")
                self.send_confirmation(data, "success")
            finally:
                if max_runs:
                    run_count += 1

    def handle_data(self, event_data):
        """Save the event_data in the DB; a
        :class:`xbus.management.commands.xbus_queue.Command`
        will process it.

        Args:
            event_data (dict): Xbus recieved data.
        """

        event_type = event_data.get("type")
        if not event_type:
            raise Exception("No event type!")

        for item in event_data.get("items", []):
            encoding = event_data.get("encoding")
            if encoding == "binary":
                item = b64decode(item)
            elif encoding != "json":
                raise Exception("Unsupported encoding")

            item = json.loads(item)

            if isinstance(item, list):
                for i in item:
                    self.save_data(event_type, i)
            else:
                self.save_data(event_type, item)

    def save_data(self, event_type, item):

        xbus_message_correlation_id = item.get("xbus_message_correlation_id")
        xref = item.get("xref")
        Event.objects.create(
            direction="in",
            state="pending",
            xbus_message_correlation_id=xbus_message_correlation_id,
            xref=xref,
            event_type=event_type,
            item=json.dumps(item),  # Yes we re-serialize...
        )

    def send_confirmation(self, data, status):
        """Send failure/success to Xbus-http Processing End

        Args:
            data (dict): Xbus recieved data.
            status (str): either "sucess" or "error"
        """

        req_data = {
            "context": data.get("context"),
            "status": status,
            "messages": [
                {
                    "timestamp": timezone.now().isoformat(),
                    "level": "NOTICE",
                    "text": "Recieved",
                }
            ],
        }

        headers = {
            "Content-Type": "application/json",
            "Xbus-API-Key": XBUS_API_KEY,
        }

        requests.post(
            XBUS_CONSUMER_PROCEND_URL, json=req_data, headers=headers
        )
