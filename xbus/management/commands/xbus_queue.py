"""
Xbus queue management command, apply handlers to envelopes and send envelope
to Xbus.
"""
# Import from the Standard Library
import json
import logging
import sys
from optparse import make_option
from os import fdopen
from time import sleep
from traceback import format_exc

# Import from Django
from django.core.management.base import NoArgsCommand

from xbus import api
# Import from here
from xbus.models import Envelope, Event

log = logging.getLogger("xbus.commands")


class Command(NoArgsCommand):
    """Retrieve envelope store in database and consume them.

    Two mode are available:

    **in**:
        Consume envelopes retrieved by
        :class:`xbus.management.commands.xbus_consumer.Command` which store
        them in database with `in` direction, and apply relevant handler.

    **out**:
        Consume envelopes store in database with `out` direction and send them
        to xbus-http.

    help::

        Usage: ./manage.py xbus_queue [options]

        Options:
          --traceback           Raise on exception
          --daemon              Daemonize
          --in=IN               limit number of incoming events to handle
          --out=OUT             limit number of outgoing events to handle
          --disable_out         Disable outgoing messages
          --disable_in          Disable incoming messages
          --version             show program's version number and exit
          -h, --help            show this help message and exit

    """

    option_list = NoArgsCommand.option_list + (
        make_option(
            "--daemon", action="store_true", default=False, help="Daemonize"
        ),
        make_option(
            "--in",
            type="int",
            help="limit number of incoming events to handle",
        ),
        make_option(
            "--out",
            type="int",
            help="limit number of outcoming events to handle",
        ),
        make_option(
            "--disable_out",
            action="store_true",
            default=False,
            help="Disable outgoing messages",
        ),
        make_option(
            "--disable_in",
            action="store_true",
            default=False,
            help="Disable incoming messages",
        ),
    )

    def queue_run_in(self, limit=None):
        """Consume envelopes retrieved by
        :class:`xbus.management.commands.xbus_consumer.Command` which store
        them in database with `in` direction, and apply relevant handler.

        Args:
            limit (int): limit number of incoming events to handle
        """
        pending = Event.objects.filter(state="pending", direction="in")
        pending = pending.order_by("pk")
        if limit is not None:
            pending = pending[:limit]

        for event in pending:
            event_type = event.event_type
            handler = api.registry.get(event_type)

            if handler is None:
                event.state = "error"
                event.comment = 'unexpected event type "%s"' % event_type
                event.save()
                continue

            try:
                item = json.loads(str(event.item))
                obj = handler(event.xref, item)
                log.debug(
                    "Processed '%s' - '%s': \n%s",
                    event.event_type,
                    event.xref,
                    str(event.item),
                )
            except Exception:
                event.state = "error"
                event.comment = format_exc()
                event.save()
                log.error("Message consumption error - IN %s", event.comment)
            else:
                event.state = "done"
                event.comment = u""  # Override previous error, may happen
                if obj is not None:
                    if hasattr(obj, "get_admin_url"):
                        event.admin_url = obj.get_admin_url()
                event.save()

            # update envelope state
            envelope = event.envelope
            if not envelope:
                continue
            items = envelope.event_set.all()
            if all(e.state != "pending" for e in items):
                if any(e.state != "done" for e in items):
                    envelope.state = "error"
                else:
                    envelope.state = "done"
                envelope.save()

    def queue_run_out(self, limit=None):
        """
        Sends (at most 100) pending events to xbus.

        Args:
            limit (int): limit number of outgoing events to handle

        Returns:
            int: the number of pending events over 100.
        """
        pending = Envelope.objects.filter(state="pending", direction="out")
        pending = pending.order_by("pk")
        if limit is not None:
            pending = pending[:limit]

        # Avoid login if there is nothing to send
        left = 0
        n = pending.count()
        if n == 0:
            return left

        if n > 100:
            pending = pending[:100]
            left = n - 100

        for envelope in pending:
            success, reply = api._xbus_send_event(envelope)
            for event in envelope.event_set.all():
                log.debug(
                    "Sent '%s' - '%s': \n%s",
                    event.event_type,
                    event.xref,
                    str(event.item),
                )

            if not success:
                event = envelope.event_set.first()
                log.error("XBUS - Connection error - OUT %s", event.comment)

            sleep(0.1)  # Wait for Xbus to digest

        return left

    def handle_noargs(self, **kw):
        """Main function, which consume and apply CLI options."""
        try:
            self.start(**kw)
        except KeyboardInterrupt:
            log.info("Stopped xbus queue")

    def start(self, **kw):
        log.info("Starting xbus queue")
        if kw["daemon"]:
            sys.stdout = fdopen(sys.stdout.fileno(), "w", 0)
            while True:
                if not kw["disable_in"]:
                    self.queue_run_in(kw["in"])

                if not kw["disable_out"]:
                    left = self.queue_run_out(kw["out"])
                else:
                    left = 0

                if left == 0:
                    sleep(5)
        else:
            if not kw["disable_in"]:
                self.queue_run_in(kw["in"])

            if not kw["disable_out"]:
                self.queue_run_out(kw["out"])
