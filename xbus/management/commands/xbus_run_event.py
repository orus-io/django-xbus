"""
A django xbus interative python shell.
"""
# Imports from libs
import json

from IPython.terminal.embed import InteractiveShellEmbed

# Imports from django
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import LabelCommand

from xbus.api import send_event
from xbus.models import Event
# Imports from xbus
from xbus.utils import get_handler, process_incoming_data

banner = (
    "Hi there. This is an interactive IPython shell."
    "Type help_xbus_commands() for the help.\n"
    "Xbus default event_type is: %s.\n"
    "Commands: create_event(xbus_dict), run_callback(xbus_dict), "
    "get_object(model, **kwargs), replay_event(event_pk)"
)

COMMAND_HELP_MSG = (
    "Apart from django stuff usually available in django-instance shell, you\n"
    "can call:\n"
    "\n"
    "- create_event(xbus_dict, [event_type]): creates an Xbus Event that\n"
    "will eventually be processed by xbus_queue, provided it is running.\n"
    "\n"
    "- run_callback(xbus_dict, [event_type]): directly calls the registered\n"
    "callback for the event_type.\n"
    "\n"
    "- replay_event(event_pk): Call an previously stored event\n"
    "In both functions, the default event_type will be the one provided to\n"
    "this command (i.e. now: %s), \n"
    "and in both functions, you'll have to provide the xbus dict.\n"
)


ipshell = InteractiveShellEmbed(
    exit_msg="Leaving Interpreter, back to program."
)


class FakeXbusAwareObject:
    # Temporary, until we can remove need for 'instance' in send_event
    # signature.
    xref = ""


class Command(LabelCommand):
    """Interactive python shell based on IPython and django shell to interact
    with xbus.

    help::

        Usage: ./manage.py xbus_run_event [options] <label label ...>

        Options:
          --traceback           Raise on exception
          --version             show program's version number and exit
          -h, --help            show this help message and exit

    *label* is an xbus event type.
    """

    def replay_event(self, pk):
        """Re-applay event handler for given event.

        Args:
            pk: :class:`xbus.models.Event` primary key.

        Returns:
            callback function result or None.

        Doesn't handle *out* event.
        """
        try:
            event = Event.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return None

        xbus_dict = json.loads(event.item)
        if event.direction == "in":
            return self.run_callback(xbus_dict, event.event_type)
        elif event.direction == "out":
            # Let's keep the thing simple
            # return self.create_event(xbus_dict, event.event_type)
            return None
        else:
            return None

    def create_event(self, xbus_dict, event_type=None):
        """Create and send an event.

        Args:
            xbus_dict (dict): data to send to xbus.
            event_type (str): xbus event type, default is *label*.

        Returns:
            :func:`xbus.api.send_event` returned value.
        """
        if event_type is None:
            event_type = self.event_type
        instance = FakeXbusAwareObject()
        instance.xref = xbus_dict["xref"]
        return send_event(instance, event_type, xbus_dict)

    def run_callback(self, xbus_dict, event_type=None):
        """Apply handler for given event type on given data.

        Args:
            xbus_dict (dict): data send to handler.
            event_type (str): xbus event type, default is *label*.

        Returns:
            :func:`xbus.utils.process_incoming_data` returned value.
        """

        if event_type is None:
            event_type = self.event_type
        return process_incoming_data(event_type, xbus_dict)

    def command_help(self):
        """Display help message"""
        print(COMMAND_HELP_MSG % self.event_type)

    def handle_label(self, label, **kw):
        """Main function to launch interactive shell"""
        self.event_type = label

        handler = get_handler(self.event_type)
        if handler is None:
            print("!warning!: unregistered event type: %s" % self.event_type)

        self.command_help
        self.create_event
        self.run_callback
        self.replay_event

        ipshell.banner1 = banner % label
        ipshell()
