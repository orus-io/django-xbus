"""
Xbus Low level API is designed to handle
`xbus-http <https://docs.xbus.io/latest/http/>`_ calls.

**xref** is a unique identifier for models, it's recommanded to use UUIDv4 for
this purpose, it enable two way synchronization between models.
"""
# Import from the Standard Library
import json
import logging
import time
from base64 import b64encode
from traceback import format_exc
from uuid import uuid4

# Import from dependencies.
import requests

# Import from this addon.
from xbus.constants import XBUS_API_KEY, XBUS_EMITTER_OUTPUT, XBUS_EMITTER_URL

# Import from Django
# from django.conf import settings

try:
    from django.db.models.loading import get_model
except ImportError:
    from django.apps import apps

    get_model = apps.get_model

registry = {}
logger = logging.getLogger(__name__)


def register_handler(event_type, handler):
    """Register a new handler for a given xbus `event_type`.

    When consumer will recieve an event of this type corresponding handler
    will be called.

    Args:
        event_type (str): Xbus Event Type.
        handler (func): callback function which take an xref and a dict which
            represent xbus recieved data as parameters.
    """
    registry[event_type] = handler


def send_event(
    instance,
    event_type,
    item,
    immediate=False,
    admin_url=None,
    run_command=False,
):
    """
    Utility function used typically by signal handlers to send an xbus event.
    For now we only support sending 1 event per envelope, and 1 item per event.

    Every time a event is send an `xbus_message_correlation_id` is affected,
    which is designed to be unique (UUIDv4).

    In case of `immediate` reply `xbus_message_correlation_id` is used to
    identify the reply.

    Args:
        instance (xbus.models.XbusAwareMixin): Django :class:`Model` subclass
            instance to send, used to retrieve `xref`.
        event_type (str): Xbus event type.
        item (dict): Explicit data send to xbus.
        immediate (bool): If True then wait and return reply.
        admin_url (str): Instance django admin URL.
        run_command (bool): If true will run consumer command before returning.

    Returns:
        tuple:
            - `xbus.models.Event`: sent event.
            - `bool or None`: Success, which is a `bool` in case of immediate
              reply else `None`.
            - `dict or None`: Reply send by xbus in case immediate reply.

    .. warning::
        `run_command` is design for test purpose and should not be used in
        production.
    """

    # Identify object and message
    xbus_message_correlation_id = str(uuid4())

    # If the instance doesn't have xref, then create it
    if not instance.xref:
        instance.xref = str(uuid4())

        if instance.pk:
            instance.save(update_fields=["xref"])

    xref = str(instance.xref)

    # Fill item
    item["xref"] = xref
    item["xbus_message_correlation_id"] = xbus_message_correlation_id

    # The broker expects unicode everywhere.
    item = {
        unicode(k): unicode(v) if type(v) is str else v
        for k, v in item.items()
    }
    event_type = unicode(event_type)

    # Serialize for the storage.
    item = json.dumps(item)

    if immediate:
        direction = "immediate-out"
    else:
        direction = "out"

    # Add to the queue
    event_model = get_model("xbus", "Event")
    envelope_model = get_model("xbus", "Envelope")
    envelope = envelope_model.objects.create(
        envelope_id=str(uuid4()), direction=direction, state="pending"
    )
    event = event_model.objects.create(
        xbus_message_correlation_id=xbus_message_correlation_id,
        direction=direction,
        xref=xref,
        event_type=event_type,
        item=item,
        admin_url=admin_url,
        envelope=envelope,
        event_id=str(uuid4()),
        state="pending",
    )

    if immediate:
        try:
            reply_event, success, reply = send_immediate_reply_event(
                envelope, xbus_message_correlation_id, run_command=run_command
            )
            event.comment = "Returned code: %s\nReturned val: %s" % (
                success,
                reply,
            )
            if success is True:
                envelope.state = "done"
                event.state = "done"
            else:
                envelope.state = "error"
                event.state = "error"

            envelope.save()
            event.save()

            return reply_event, success, reply
        except Exception:
            envelope.state = "error"
            event.state = "error"
            event.comment = format_exc()

            envelope.save()
            event.save()

            return event, False, None

    return event, None, None


def send_immediate_reply_event(
    envelope, xbus_message_correlation_id, run_command=False
):
    """
    Send immediate reply event

    Args:
        envelope (xbus.models.Envelope): Sent envelope.
        xbus_message_correlation_id (str): is used to identify the reply.
        run_command (bool): If true will run consumer command before returning.

    Returns:
        tuple:
            - `bool`: Transaction success.
            - `dict or None`: Reply send by xbus `None` on failure.

    .. warning::
        `run_command` is design for test purpose and should not be used in
        production.
    """
    event_model = get_model("xbus", "Event")
    success, reply = _xbus_send_event(envelope)
    if not success:
        return success, None

    if run_command:  # use for test but can be useful
        from django.core.management import call_command

        call_command("xbus_consumer", max_runs=1)

    count = 0
    while True:
        event = event_model.objects.filter(
            xbus_message_correlation_id=xbus_message_correlation_id,
            direction="in",
        )
        if event:
            event = event[0]
            event.state = "done"
            event.save()
            return event, success, json.loads(str(event.item))
        if count > 10:
            break
        else:
            count += 1
        time.sleep(1)

    return None, False, None


def _xbus_send_event(envelope):
    """
    Returns a tuple with three values:

    Args:
        envelope (xbus.models.Envelope): Sent envelope.

    Returns:
        tuple:
            - success (bool) : True if the operation succeeded, False if not
            - reply (dict): returned value for immediate-reply, None otherwise
            - event_id (str): Last event-id, for debugging purpuses
    """

    # Prepare headers for POST requests towards Xbus-HTTP.
    headers = {
        "Content-Type": "application/json",
        "Xbus-API-Key": XBUS_API_KEY,
    }

    logger.info("Sending the envelope %s to Xbus...", envelope.pk)

    # Build a message containing all events in the envelope.
    # Note: We only support 1 item per event.
    events = envelope.event_set.all()
    req_data = {
        "output": XBUS_EMITTER_OUTPUT,
        "envelope": {
            "id": envelope.envelope_id,
            "event_ids": [event.event_id for event in events],
            "events": [
                {
                    "id": event.event_id,
                    "ItemCount": 1,
                    "encoding": "binary",
                    "items": [b64encode(event.item)],
                    "type": event.event_type,
                }
                for event in events
            ],
        },
    }
    try:
        req = requests.post(XBUS_EMITTER_URL, json=req_data, headers=headers)
    except Exception:
        envelope.state = "error"
        for event in events:
            event.comment = format_exc()
            event.state = "error"
            event.save()

        success = False
        reply = ""
    else:
        success = req.status_code == requests.codes.ok
        reply = req.text
        envelope.state = "done"
        for event in events:
            event.state = "done"
            event.save()
        envelope.comment = reply

    envelope.save()

    return success, reply
