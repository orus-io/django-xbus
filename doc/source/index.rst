.. django-xbus documentation master file, created by
   sphinx-quickstart on Tue Jan 15 15:34:18 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

django-xbus
===========

Utilities to synchronize django models with
`Xbus <https://docs.xbus.io/latest/>`_ via
`Xbus HTTP <https://docs.xbus.io/latest/http/>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   xbus-http
   models
   api
   context_manager
   utils
   admin
   commands




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
