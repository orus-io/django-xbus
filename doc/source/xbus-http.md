# Configuration

Communication with [Xbus][1] is handle by [Xbus HTTP][2] so you need to create
an `Xbus` account and have an API key to comminicate with `Xbus HTTP.

## Create an Xbus account

First create a configuration file (say, "django.yaml") suitable for
xbus-client with such an account definition:

```yaml
account-name: django-app
nats-url: nats://xbusd:4222
actors:
  "0":
    name: "django-emitter"
    kind: emitter
    roles: [django]
  "1":
    name: "django-consumer"
    kind: consumer
    roles: [django]
```

Then use [xbus-client][3] to register the account, then [xbusctl][4] to accept
the account and the actors:

```console
$ xbus-client --config django.yaml register
[12] [INF] Loading actor actors.0.roles options
[12] [INF] Loading actor actors.1.roles options
[12] [INF] Anonymous connection
[12] [INF] Successfully sent the registration request.
[12] [INF] The registration is pending approval. To approve from Xbus, run:

        xbusctl account accept "django-app"
        xbusctl actor accept "django-emitter"
        xbusctl actor accept "django-consumer"

Then run the exact same command you just runned (register)
$ xbusctl account accept django-app
[22] [INF] Successfully accepted the account "1d28a384-347c-4718-9c4d-aab51009f5f5".
$ xbusctl actor accept django-emitter
[32] [INF] Successfully accepted the actor "django-emitter" (ID "0213a67c-c6da-49a7-96c2-ceca35e2425a").
$ xbusctl actor accept django-consumer
[42] [INF] Successfully accepted the actor "django-consumer" (ID "979de18f-0e1a-4d6c-bdc8-2f6bada998d1").
```

Checkout [xbusctl documentation][4] for more options, like list actors and
accounts.

## Get your account API key

```console
$ xbusctl account renew-apikey django-app
HVJzXnpvKfVB8T3gYslg2sLMF6WW140i
```

More information can be found on [Xbus HTTP documentation][2].

## Update your django settings

Now use those informations to update your django settings:

```py
XBUS_API_KEY = "HVJzXnpvKfVB8T3gYslg2sLMF6WW140i"
XBUS_HTTP_URL = "http://127.0.0.1:8911"  # This is Xbus HTTP URL
XBUS_EMITTER_NAME = "django-emitter"
XBUS_CONSUMER_NAME = "django-consumer"
XBUS_EMITTER_OUTPUT = "default"  # This is the default value
```

Alternatively you can set those settings using environment variables, using
same names, but remember that django settings takes precedences.

## Create and activate your piplines

If you followed the [quickstart][5] guide then and have a backend xbus account
defined like the following:

```yaml
account-name: back-app
nats-url: nats://xbusd:4222
actors:
  "0":
    name: "back-emitter"
    kind: emitter
    roles: [back]
  "1":
    name: "back-consumer"
    kind: consumer
    roles: [back]
```

You can defined pipelines like so:

- For `Post` created **from django**

```yaml
# filename: django-to-back-post_created.yaml
nodes:
- id: source
  type: emitter
  actorids: []
  actors: []
  roles:
  - django
  rolebroadcast: true
  sourcematch:
    eventtypes:
    - front_post_created
  inputs: []
  outputs:
  - default
- id: sink
  type: consumer
  actorids: []
  actors: []
  roles:
  - back
  rolebroadcast: true
  sourcematch: null
  inputs:
  - default
  outputs: []
edges:
- source.default->sink.default
```

- For `Post` updated **from back**

```yaml
# filename: back-to-django-post_updated.yaml
nodes:
- id: source
  type: emitter
  actorids: []
  actors: []
  roles:
  - back
  rolebroadcast: true
  sourcematch:
    eventtypes:
    - back_post_updated
  inputs: []
  outputs:
  - default
- id: sink
  type: consumer
  actorids: []
  actors: []
  roles:
  - django
  rolebroadcast: true
  sourcematch: null
  inputs:
  - default
  outputs: []
edges:
- source.default->sink.default
```

Then register your pipelines using the following [xbusctl][4] commands:

```console
$ xbusctl pipeline save django-to-back-post_created \
	./django-to-back-post_created.yaml
[33] [INF] Successfully saved pipeline django-to-back-post_created- graph
$ xbusctl pipeline save back-to-django-post_updated \
	./back-to-django-post_updated.yaml
[43] [INF] Successfully saved pipeline back-to-django-post_updated- graph
```

Now don't forget to activate your pipelines:

```console
$ xbusctl pipeline status django-to-back-post_created --active
[81] [INF] django-to-back-post_created- status changed to ACTIVE
$ xbusctl pipeline status back-to-django-post_updated --active
[91] [INF] back-to-django-post_updated- status changed to ACTIVE
```

Checkout [xbusctl documentation][4] and/or `xbusctl --help` for more
informations.

[1]: https://docs.xbus.io/latest/
[2]: https://docs.xbus.io/latest/http/
[3]: https://docs.xbus.io/latest/xbus-client.html
[4]: https://docs.xbus.io/latest/xbusctl.html
[5]: /quickstart.html
