# Quickstart

Working with django xbus consist of few steps:

- Declaring your models and synchronized fields
- Consuming xbus messages
- Sending and applying callback on xbus messages
- Recieving xbus messages

## Declaring your xbus aware models

To do so you just needs to subclass `xbus.models.XbusAwareMixin` and defined
required methodes, eg.:

```python
from django.db import models
from django.core.urlresolvers import reverse
from xbus.models import XbusAwareMixin


class Post(XbusAwareMixin, models.Model):

    title = models.CharField(max_length=255)
    content = models.TextField()

    def get_xbus_fields(self):
        return {
            "title": self.title,
            "content": self.content,
        }

    @staticmethod
    def get_xbus_event_type(event_type):
        return "front_post_{}".format(event_type)

    def get_admin_url(self):
        return reverse(
            'admin:{}_{}_change'.format(
                self._meta.app_label,
                self._meta.model_name
            ),
            args=[self.id]
        )
```

Now every time a `Post` instance will be created or updated a
`xbus.models.Event` and a `xbus.models.Envelope` will be created with the
corresponding payload and event type.

In case of creation event type will be `front_post_created` and in case of
update `front_post_updated`.

## Consuming xbus messages

In order to consume xbus messages you should implement an handler for every
xbus event type you want to handle.

To do so use `xbus.api.register_handler`. eg.:

```python
from xbus.utils import get_object, update_object
from xbus.api import register_handler


def post_callback(xref, data):
    instance = get_object(Post, xref=xref)
    if instance:
        update_object(instance, **data)
    else:
        instance = Post.objects.create(**data)
    return instance


register_handler("back_post_created", post_callback)
register_handler("back_post_updated", post_callback)
```

Here we define the same callback for two xbus event type.

## Sending and applying callback on xbus messages

Use `xbus.management.commands.xbus_queue` to send messages to xbus and also
apply callback on recieved messages. You can let this command work in
background, eg.:

```sh
$ ./manage.py xbus_queue --daemon
```

## Recieving xbus messages

Use `xbus.management.commands.xbus_consumer` to recieve messages from xbus, it
will store messages in database which late will be consume by `xbus_queue`
command. You can let this command work in background, eg.:

```sh
$ ./manage.py xbus_consumer
```
